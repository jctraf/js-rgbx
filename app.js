console.log("HELLO");

// FUNCTIONS
const redButtonPressed = function() {
    console.log("RED BUTTON WAS PRESSED!");

    // change background color
    // -----
    // 1. GETT THE BODY TAG
    let body = document.querySelector("body");

    // 2. CHANGE THE BCKGROUND COLOR
    body.style.backgroundColor="#ff0000";
}
const green = function() {
    console.log("GREEEN");
    let body = document.querySelector("body");
    body.style.backgroundColor="#00ff00";
}

const blue = function() {
    console.log("BLUE");
    let body = document.querySelector("body");
    body.style.backgroundColor="#0000ff";
}

const xButtonPressed = function() {
    console.log("X button");
    //1. get the value form the input box
    let inputBox = document.querySelector("input");
    //2. change the background color to be that hext value
    let color = inputBox.value;
    document.querySelector("body").style.backgroundColor=color;

}

// CLICK HANDLERS
document.getElementById("redButton").addEventListener("click",redButtonPressed);
document.getElementById("greenButton").addEventListener("click", green);
document.getElementById("blueButton").addEventListener("click", blue);
document.getElementById("xButton").addEventListener("click", xButtonPressed);

